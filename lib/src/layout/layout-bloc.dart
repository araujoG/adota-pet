import 'dart:async';

import 'package:adota_pet/src/pages/home-widget.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:adota_pet/src/services/authentication/Authentication.dart';
import 'package:rxdart/rxdart.dart';

class LayoutBloc extends BlocBase {
  
  var _controllerLoading = BehaviorSubject<bool>(seedValue: false);
  Stream<bool> get outLoading => _controllerLoading.stream;

  final _authentication = new Authentication();
  
  final BuildContext context;
  LayoutBloc(this.context);


  Future changeAcount(BuildContext context) async{
    _authentication.changeAccount(context);
  }

  FutureOr goHome() {
    Navigator.of(context).pushReplacementNamed(HomeWidget.tag);
  }

  @override
  void dispose() {
  }
  
}
import 'dart:io';

import 'package:adota_pet/src/pages/about.dart';
import 'package:adota_pet/src/pages/chips-filtro.dart';
import 'package:adota_pet/src/pages/home-widget.dart';
import 'package:adota_pet/src/pages/new-pet.dart';
import 'package:adota_pet/src/pages/my-pets.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:adota_pet/src/layout/layout-bloc.dart';
import 'package:adota_pet/src/services/authentication/Authentication.dart';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';


alterarDados(BuildContext context) async {
  final baseTextStyle = const TextStyle(fontFamily: 'Poppins');
  final regularTextStyle = baseTextStyle.copyWith(
      color: Layout.font(), fontSize: 14.0, fontWeight: FontWeight.w400);
  final headerTextStyle = baseTextStyle.copyWith(
      color: Layout.font(), fontSize: 25.0, fontWeight: FontWeight.w600);

  DocumentSnapshot doc = await Firestore.instance
      .collection('usuarios')
      .document(Authentication.usuarioLogado.uid)
      .get();

  TextEditingController _ctelefone = TextEditingController();
  _ctelefone.text = doc['telefone'];

  TextEditingController _cnome = TextEditingController();
  _cnome.text = doc['nome'];

  TextFormField inputNome = TextFormField(
    keyboardType: TextInputType.text,
    controller: _cnome,
    autofocus: true,
    decoration: InputDecoration(
      hintStyle: regularTextStyle,
      labelStyle: regularTextStyle,
      hintText: 'Nome',
      labelText: 'Nome',
      contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
      ),
    ),
    validator: (value) {
      if (value.isEmpty) {
        return "Obrigatório";
      }
      return null;
    },
  );

  TextFormField inputTel = TextFormField(
    keyboardType: TextInputType.phone,
    controller: _ctelefone,
    autofocus: true,
    decoration: InputDecoration(
      hintStyle: regularTextStyle,
      labelStyle: regularTextStyle,
      hintText: 'Telefone',
      labelText: 'Telefone',
      contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
      ),
    ),
    validator: (value) {
      if (value.isEmpty) {
        return "Obrigatório";
      }
      return null;
    },
  );
  RaisedButton salvar = RaisedButton(
    child: Text(
      "Salvar",
      style: TextStyle(color: Layout.white()),
    ),
    onPressed: () {
      Firestore.instance
          .collection('usuarios')
          .document(Authentication.usuarioLogado.uid)
          .setData({
        'telefone': _ctelefone.text,
        'nome': _cnome.text,
      }, merge: true);
      _ctelefone.text = "";
      Navigator.of(context).pop();
    },
    color: Layout.secundary(),
  );

  AlertDialog displayContato = AlertDialog(
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0))),
    content: SingleChildScrollView(
      child: ListBody(
        children: <Widget>[
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              image: DecorationImage(
                image: NetworkImage(doc["foto"]),
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
          Row(children: <Widget>[
            Icon(FontAwesomeIcons.at),
            Text(":  ", style: headerTextStyle),
            Expanded(
              child: Text(
                doc['email'],
                maxLines: 2,
                style: regularTextStyle,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ]),
          Container(
            height: 5,
          ),
          inputTel,
          Container(
            height: 15,
          ),
          inputNome
        ],
      ),
    ),
    actions: <Widget>[salvar],
  );
  showDialog(
    context: context,
    builder: (BuildContext ctx) {
      return displayContato;
    },
  );
}

editar(BuildContext context) {
  return IconButton(
    icon: Icon(FontAwesomeIcons.pen),
    onPressed: () {
      Navigator.of(context).pop();
      alterarDados(context);
    },
    color: Layout.secundary(),
  );
}

infoContato(BuildContext context, String uid) async {
  final baseTextStyle = const TextStyle(fontFamily: 'Poppins');
  final regularTextStyle = baseTextStyle.copyWith(
      color: Layout.font(), fontSize: 16.0, fontWeight: FontWeight.w400);
  final headerTextStyle = baseTextStyle.copyWith(
      color: Layout.font(), fontSize: 25.0, fontWeight: FontWeight.w600);

  DocumentSnapshot doc =
      await Firestore.instance.collection('usuarios').document(uid).get();
  AlertDialog displayContato = AlertDialog(
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0))),
    content: SingleChildScrollView(
      child: ListBody(
        children: <Widget>[
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              image: DecorationImage(
                image: NetworkImage(doc["foto"]),
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
          Center(
            child: Text(title(doc['nome']), style: headerTextStyle),
          ),
          Row(children: <Widget>[
            Icon(FontAwesomeIcons.at),
            Text(":  ", style: headerTextStyle),
            Expanded(
              child: Text(
                doc['email'],
                maxLines: 2,
                style: regularTextStyle,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ]),
          Row(children: <Widget>[
            Icon(FontAwesomeIcons.phoneSquareAlt),
            Text(":  ", style: headerTextStyle),
            Expanded(
              child: Text(
                doc['telefone'],
                maxLines: 2,
                style: regularTextStyle,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ]),
        ],
      ),
    ),
    actions: <Widget>[editar(context)],
  );

  showDialog(
    context: context,
    builder: (BuildContext ctx) {
      return displayContato;
    },
  );
}

class Layout {
  static TextStyle baseTextStyle = const TextStyle(fontFamily: 'Poppins');
  static TextStyle regularTextStyle = baseTextStyle.copyWith(
      color: Layout.font(), fontSize: 14.0, fontWeight: FontWeight.w400);
  static TextStyle subHeaderTextStyle =
      regularTextStyle.copyWith(fontSize: 17.0);
  static TextStyle headerTextStyle = baseTextStyle.copyWith(
      color: Layout.font(), fontSize: 25.0, fontWeight: FontWeight.w600);

  static _filtrosLocal() {
    List<FiltroD> distancias = [
      FiltroD(5),
      FiltroD(10),
      FiltroD(20),
      FiltroD(30),
      FiltroD(40),
      FiltroD(50),
      FiltroD(100),
      FiltroD(200),
      FiltroD(300),
    ];
    return FiltroDistancia(distancias);
  }

  static _filtrosIdade() {
    List<FiltroI> idades = [
      FiltroI(1),
      FiltroI(2),
      FiltroI(3),
      FiltroI(4),
      FiltroI(5),
      FiltroI(6),
    ];
    return FiltroIdade(idades);
  }

  static _filtrosSexo() {
    List<FiltroS> sexos = [FiltroS(0), FiltroS(1)];
    return FiltroSexo(sexos);
  }

  static _filtrosTipo() {
    List<FiltroT> tipos = List();
    int i = 0;
    for (var item in Tipos) {
      tipos.add(FiltroT(item, i++));
    }
    return FiltroTipo(tipos);
  }

  static _aplicarFiltros(BuildContext context) => RaisedButton(
        child: Text(
          "Aplicar Filtros",
          style: TextStyle(color: Layout.white()),
        ),
        onPressed: () {
          if (FiltroD.selecionado != null)
            HomeWidget.distancia = FiltroD.selecionado.valor;
          if (FiltroS.selecionado != null)
            HomeWidget.sexo = FiltroS.selecionado.valor;
          if (FiltroT.selecionado != null)
            HomeWidget.tipo = FiltroT.selecionado.valor;
          if (FiltroI.selecionado != null)
            HomeWidget.idade = FiltroI.selecionado.valor;
          Navigator.of(context).pop();
          Navigator.of(context).popAndPushNamed(HomeWidget.tag);
        },
        color: Layout.secundary(),
      );

  static _limparFiltros(BuildContext context) => RaisedButton(
        child: Text(
          "Limpar Filtros",
          style: TextStyle(color: Layout.white()),
        ),
        onPressed: () {
          HomeWidget.distancia = -1;
          FiltroD.selecionado = null;
          HomeWidget.sexo = -1;
          FiltroS.selecionado = null;
          HomeWidget.tipo = -1;
          FiltroT.selecionado = null;
          HomeWidget.idade = -1;
          FiltroI.selecionado = null;
          Navigator.of(context).pop();
          Navigator.of(context).popAndPushNamed(HomeWidget.tag);
        },
        color: Layout.accent(),
      );

  static showAlertDialog1(BuildContext context) {
    // configura o  AlertDialog
    AlertDialog alerta = AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "Localização: ",
                  style: headerTextStyle,
                )
              ],
            ),
            _filtrosLocal(),
            Divider(),
            Row(
              children: <Widget>[
                Text(
                  "Sexo: ",
                  style: headerTextStyle,
                )
              ],
            ),
            _filtrosSexo(),
            Divider(),
            Row(
              children: <Widget>[
                Text(
                  "Tipo: ",
                  style: headerTextStyle,
                )
              ],
            ),
            _filtrosTipo(),
            Divider(),
            Row(
              children: <Widget>[
                Text(
                  "Idade: ",
                  style: headerTextStyle,
                )
              ],
            ),
            _filtrosIdade()
          ],
        ),
      ),
      actions: [_limparFiltros(context), _aplicarFiltros(context)],
    );

    // exibe o dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alerta;
      },
    );
  }

  static File sampleImage;
  static String nomeImagem;

  static Future getImage() async {
    Layout.sampleImage =
        await ImagePicker.pickImage(source: ImageSource.gallery);
    Layout.nomeImagem = basename(sampleImage.path);
  }

  static Future<dynamic> uploadPic(BuildContext context) async {
    String fileName = basename(sampleImage.path);
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(sampleImage);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    return taskSnapshot.ref.getDownloadURL();
  }

  static Scaffold getContent(BuildContext context, content, tag) {
    LayoutBloc bloc = BlocProvider.of<LayoutBloc>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary(),
        title: Text(
          "Adota Pet",
          style: TextStyle(
            fontSize: 25,
            color: Layout.white(),
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      drawer: Drawer(
        child: Container(
          color: light(),
          child: ListView(
            children: <Widget>[
              _header(bloc, context),
              _menuItem(
                  "Home", Icon(FontAwesomeIcons.home), HomeWidget.tag, context),
              ListTile(
                dense: false,
                leading: Icon(FontAwesomeIcons.userAlt),
                title: Text("Meu perfil"),
                onTap: () {
                  infoContato(context, Authentication.usuarioLogado.uid);
                },
              ),
              _menuItem("Meus Pets", Icon(FontAwesomeIcons.paw),
                  MyPetsWidget.tag, context),
              _menuItem("Novo Pet", Icon(FontAwesomeIcons.plus),
                  FormularioPetWidget.tag, context),
              _menuItem("Sobre", Icon(FontAwesomeIcons.infoCircle),
                  AboutPage.tag, context),
            ],
          ),
        ),
      ),
      backgroundColor: Layout.light(),
      body: content,
      floatingActionButton: _filterButton(tag, context), //parametro do scaffold
    );
  }

  static _filterButton(String tag, BuildContext context) {
    if (tag == "home-page")
      return FloatingActionButton(
          child: Icon(FontAwesomeIcons.filter),
          backgroundColor: Layout.primary(),
          onPressed: () {
            showAlertDialog1(context);
          });
    else
      return Container();
  }

  static _cadastrarPet(BuildContext context) {
    return <Widget>[
      GestureDetector(
        child: Icon(FontAwesomeIcons.paw),
        onTap: () {
          Navigator.of(context).pushNamed(FormularioPetWidget.tag);
        },
      ),
      Padding(
        padding: EdgeInsets.only(right: 20),
      )
    ];
  }

  static _header(LayoutBloc bloc, BuildContext context) {
    return UserAccountsDrawerHeader(
      accountName: Text(Authentication.usuarioLogado.displayName,
          style: TextStyle(color: white())),
      accountEmail: Text(Authentication.usuarioLogado.email,
          style: TextStyle(color: white())),
      currentAccountPicture: GestureDetector(
        child: CircleAvatar(
          backgroundImage: NetworkImage(Authentication.usuarioLogado.photoUrl),
        ),
        onTap: () {
          bloc.changeAcount(context);
        },
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage('assets/img/header.jpg'),
        ),
      ),
    );
  }

  static _menuItem(
      String nome, Icon icone, String paginaTag, BuildContext context) {
    return ListTile(
      dense: false,
      leading: icone,
      title: Text(nome),
      onTap: () {
        Navigator.of(context).pop();
        Navigator.of(context).pushNamed(paginaTag);
      },
    );
  }

  static Color primary([double opacity = 1]) =>
      Color.fromRGBO(255, 119, 124, opacity); //appbar
  static Color light([double opacity = 1]) =>
      Color.fromRGBO(254, 225, 225, opacity); //fundo
  static Color white([double opacity = 1]) =>
      Color.fromRGBO(255, 255, 255, opacity); //branco
  static Color font([double opacity = 1]) =>
      Color.fromRGBO(25, 25, 25, opacity); //cinza
  static Color card([double opacity = 1]) =>
      Color.fromRGBO(240, 240, 240, 1); //cinza

  static Color secundary([double opacity = 1]) =>
      Color.fromRGBO(31, 22, 86, opacity); //destaque1
  static Color accent([double opacity = 1]) =>
      Color.fromRGBO(153, 49, 56, opacity); //contrate1
}

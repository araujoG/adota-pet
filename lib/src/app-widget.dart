import 'package:adota_pet/src/app-bloc.dart';
import 'package:adota_pet/src/login/login-widget.dart';
import 'package:adota_pet/src/pages/about.dart';
import 'package:adota_pet/src/pages/home-widget.dart';
import 'package:adota_pet/src/pages/map.dart';
import 'package:adota_pet/src/pages/new-pet.dart';
import 'package:adota_pet/src/pages/my-pets.dart';
import 'package:adota_pet/src/pages/settings.dart';
import 'package:adota_pet/src/pages/upload.dart';
import 'package:flutter/material.dart';
import 'package:bloc_pattern/bloc_pattern.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<AppBloc>(
      bloc: AppBloc(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          ),
        home: LoginWidget(),
        initialRoute: '/',
        routes: {
          'home-page':(context) => HomeWidget(),
          'about-page':(context) => AboutPage(),
          'settings-page':(context) => SettingsPage(),
          'upload-page':(context) => UploadPage(),
          'newPet-page':(context) => FormularioPetWidget(),
          'mypets-page':(context) => MyPetsWidget(),
          'map':(context) => FireMap()
        },
      ),
    );
  }
}
      
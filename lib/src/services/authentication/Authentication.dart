import 'package:adota_pet/src/pages/home-widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:async';

class Authentication {
  final FirebaseAuth _firebase =
      FirebaseAuth.instance; //variaveld de acesso a todo firebase
  final _google = new GoogleSignIn();

  static FirebaseUser usuarioLogado;

  static Map<String, double> minhaLocalizacao;

  Future<bool> signWithGoogle() async {
    getposition();
    final googleAuthentication = await _google.signIn();
    final authenticated = await googleAuthentication.authentication;

    final usuarioAutenticado = await _firebase.signInWithGoogle(
        idToken: authenticated?.idToken,
        accessToken: authenticated?.accessToken);

    Authentication.usuarioLogado = usuarioAutenticado;
    DocumentSnapshot doc = await Firestore.instance
        .collection('usuarios')
        .document(Authentication.usuarioLogado.uid)
        .get();
    if (!doc.exists)
      Firestore.instance
          .collection('usuarios')
          .document(Authentication.usuarioLogado.uid)
          .setData({
        'telefone': "",
        'nome': Authentication.usuarioLogado.displayName,
        'foto': Authentication.usuarioLogado.photoUrl,
        'email': Authentication.usuarioLogado.email,
      }, merge: true);

    return usuarioAutenticado?.uid != null;
  }

  Future<bool> changeAccount(BuildContext context) async {
    getposition();
    await _firebase.signOut();
    await _google.signOut();
    final googleAuthentication = await _google.signIn();
    final authenticated = await googleAuthentication.authentication;

    final usuarioAutenticado = await _firebase.signInWithGoogle(
        idToken: authenticated?.idToken,
        accessToken: authenticated?.accessToken);

    Authentication.usuarioLogado = usuarioAutenticado;
    DocumentSnapshot doc = await Firestore.instance
        .collection('usuarios')
        .document(Authentication.usuarioLogado.uid)
        .get();
    if (!doc.exists)
      Firestore.instance
          .collection('usuarios')
          .document(Authentication.usuarioLogado.uid)
          .setData({
        'telefone': "",
        'nome': Authentication.usuarioLogado.displayName,
        'foto': Authentication.usuarioLogado.photoUrl,
        'email': Authentication.usuarioLogado.email,
      }, merge: true);
    while (Navigator.canPop(context)) {
      Navigator.of(context).pop();
    }
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (BuildContext context) => HomeWidget()));
    return usuarioAutenticado?.uid != null;
  }

  signOutWithGoogle() async {
    getposition();
    await _firebase.signOut();
    await _google.signOut();
  }

  /* Future<bool> signWithPhone(String verificacaoId, String codigoSms) async {

    final loginResult = await _firebase.signInWithPhoneNumber(verificationId: verificacaoId, smsCode: codigoSms);
    if(loginResult?.uid != null){
      return true;
    }else{
      return false;
    }
  
  } */

}

import 'package:adota_pet/src/login/login-bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginBloc>(
      bloc: LoginBloc(context),
      child: _LoginContent(),
    );
  }
}

class _LoginContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LoginBloc bloc = BlocProvider.of<LoginBloc>(context);

    _butoes() {
      return Column(
        children: <Widget>[
          RaisedButton.icon(
            icon: Icon(FontAwesomeIcons.google),
            label: Text("Login com Google    "),
            textColor: Colors.white,
            color: Colors.red,
            onPressed: bloc.onClickGoogle,
          ),
        ],
      );
    }

    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
          margin: EdgeInsets.symmetric(vertical: 0),
          alignment: FractionalOffset.centerLeft,
          height: 100.0,
          width: 280.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            image: DecorationImage(
              image: AssetImage("assets/img/logo.png"),
              fit: BoxFit.contain,
            ),
          ),
        ),
          Container(
            height: 50,
          ),
          StreamBuilder(
              stream: bloc.outLoading,
              initialData: false,
              builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                return AnimatedCrossFade(
                  firstChild: _butoes(),
                  secondChild: Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: CircularProgressIndicator(),
                  ),
                  duration: Duration(
                    milliseconds: 500,
                  ),
                  crossFadeState: snapshot.data
                      ? CrossFadeState.showSecond
                      : CrossFadeState.showFirst,
                );
              }),
        ],
      ),
    );
  }
}

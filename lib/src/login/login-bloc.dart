import 'package:adota_pet/src/pages/home-widget.dart';
import 'package:adota_pet/src/services/authentication/Authentication.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';

class LoginBloc extends BlocBase {
  
  var _controllerLoading = BehaviorSubject<bool>(seedValue: false);
  Stream<bool> get outLoading => _controllerLoading.stream;

  final _authentication = new Authentication();

  final BuildContext context;
  LoginBloc(this.context);

  onClickFacebook(){

  }

  onClickGoogle() async {
    _controllerLoading.add(true);
    if(await _authentication.signWithGoogle()){
      Navigator.pushReplacement(context, 
        MaterialPageRoute(builder: (BuildContext context) => HomeWidget())
      );
    }else{
      _controllerLoading.add(false);
    }
  }

  onClickTelefone() async {
    _controllerLoading.add(!_controllerLoading.value);
    await Future.delayed(Duration(seconds: 2));
    _controllerLoading.add(!_controllerLoading.value);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomeWidget()));
  }
  
  @override
  void dispose() {
    _controllerLoading.close();
  }
  
}
import 'package:adota_pet/src/services/authentication/Authentication.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class FireMap extends StatefulWidget {
  
	@override
	State createState() => FireMapState();
}


class FireMapState extends State<FireMap> {

	GoogleMapController mapController;
	Location location = new Location();
  Geoflutterfire geo = Geoflutterfire();
  Firestore firestore = Firestore.instance;
	static GeoFirePoint point;

	@override
	build(context) {
		return Stack(
			children: <Widget>[
				GoogleMap(
					initialCameraPosition: CameraPosition(target: LatLng(Authentication.minhaLocalizacao['latitude'],Authentication.minhaLocalizacao['longitude']), zoom: 17),
					onMapCreated: _onMapCreated,
					myLocationEnabled: true, // Add little blue dot for device location, requires permission from user
					mapType: MapType.hybrid,
					trackCameraPosition: true,					
				),
        Positioned(
          top: (MediaQuery.of(context).size.height)/2-40,
        right: (MediaQuery.of(context).size.width-40)/ 2,
        child: Icon(FontAwesomeIcons.mapMarkerAlt, size: 40.0, color: Color.fromRGBO(255, 119, 124, 1))
        ),
				Positioned(
					bottom: 50,
					right: 10,
					child: 
					FloatingActionButton(
						child: Icon(Icons.add,color: Color.fromRGBO(255, 255, 255, 1),),
						backgroundColor: Color.fromRGBO(255, 119, 124, 1),
						onPressed: () => _addMarker()
					)
				),
			],
		);
	}

	void _addMarker(){
		var marker = MarkerOptions(
		position: mapController.cameraPosition.target,
		icon: BitmapDescriptor.defaultMarker,
		);

		point = geo.point(latitude: marker.position.latitude, longitude: marker.position.longitude);

		mapController.addMarker(marker);
		Navigator.of(context).pop();
	}

	void _onMapCreated(GoogleMapController controller) {
		setState(() {
			mapController = controller;
		});
	}

/*   _animateToUser() async {
    var pos = await location.getLocation();

    mapController.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
          target: LatLng(pos.latitude, pos.latitude),
          zoom: 17.0,
        )
      )
    );
  } */
}
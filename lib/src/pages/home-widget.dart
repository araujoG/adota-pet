import 'dart:core';
import 'dart:core' as prefix0;
import 'dart:io';
import 'package:adota_pet/src/pages/settings.dart';
import 'package:flutter/material.dart' as prefix1;
import 'package:geoflutterfire/geoflutterfire.dart' as ponto;
import 'package:adota_pet/src/layout/layout-bloc.dart';
import 'package:adota_pet/src/layout/layout.dart';
import 'package:adota_pet/src/pages/editar-pet.dart';
import 'package:adota_pet/src/pages/map.dart';
import 'package:adota_pet/src/pages/pet-profile.dart';
import 'package:adota_pet/src/services/authentication/Authentication.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:adota_pet/src/pages/pet-profile.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:adota_pet/src/pages/chips-filtro.dart';

final baseTextStyle = const TextStyle(fontFamily: 'Poppins');
final regularTextStyle = baseTextStyle.copyWith(
    color: Layout.font(), fontSize: 14.0, fontWeight: FontWeight.w400);
final subHeaderTextStyle = regularTextStyle.copyWith(fontSize: 17.0);
final headerTextStyle = baseTextStyle.copyWith(
    color: Layout.font(), fontSize: 25.0, fontWeight: FontWeight.w600);

_dado(DocumentSnapshot doc, String label) {
  String s;
  if (doc != null)
    s = doc[label];
  else
    s = "";
  return Row(
    children: <Widget>[
      Text(title(label) + ": ", style: headerTextStyle),
      Text(
        s,
        style: regularTextStyle,
      )
    ],
  );
}

TextEditingController _ctelefone = TextEditingController();

_inputTel() {
  _ctelefone.text = '';

  TextFormField inputTel = TextFormField(
    keyboardType: TextInputType.phone,
    controller: _ctelefone,
    autofocus: true,
    decoration: InputDecoration(
      hintStyle: regularTextStyle,
      labelStyle: regularTextStyle,
      hintText: 'Telefone',
      labelText: 'Telefone',
      contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
      ),
    ),
    validator: (value) {
      if (value.isEmpty) {
        return "Obrigatório";
      }
      return null;
    },
  );
}

_salvar(BuildContext context) => RaisedButton(
      child: Text(
        "Salvar",
        style: TextStyle(color: Layout.white()),
      ),
      onPressed: () {
        Firestore.instance
            .collection('usuarios')
            .document(Authentication.usuarioLogado.uid)
            .setData({
          'telefone': _ctelefone.text,
          'nome': Authentication.usuarioLogado.displayName,
        }, merge: true);
        _ctelefone.text = "";
        Navigator.of(context).pop();
      },
      color: Layout.secundary(),
    );

_cadastrarContato(BuildContext context) {
  AlertDialog alterarContato = AlertDialog(
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0))),
    content: SingleChildScrollView(
      child: ListBody(
        children: <Widget>[
          Container(
            height: 5,
          ),
          _inputTel()
        ],
      ),
    ),
    actions: <Widget>[_salvar(context)],
  );
  showDialog(
    context: context,
    builder: (BuildContext ctx) {
      return alterarContato;
    },
  );
}

_infoContato(BuildContext context, String uid) async {
  DocumentSnapshot doc =
      await Firestore.instance.collection('usuarios').document(uid).get();
  AlertDialog displayContato = AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                image: DecorationImage(
                  image: NetworkImage(doc["foto"]),
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            _dado(doc, "nome"),
            _dado(doc, "email"),
            _dado(doc, "telefone")
          ],
        ),
      ));
  showDialog(
    context: context,
    builder: (BuildContext ctx) {
      return displayContato;
    },
  );
}

enum ListAction { editar, deletar }

var Tipos = [
  "Cachorro",
  "Gato",
  "Ave",
  "Peixe",
  "Roedor",
  "Coelho",
  "Equino",
  "Outros"
];

String title(String s) => s[0].toUpperCase() + s.substring(1).toLowerCase();

distancia(DocumentSnapshot doc) {
  GeoPoint g = doc['localizacao']['geopoint'];
  double d = ponto.GeoFirePoint.distanceBetween(
      to: ponto.Coordinates(g.latitude, g.longitude),
      from: ponto.Coordinates(Authentication.minhaLocalizacao['latitude'],
          Authentication.minhaLocalizacao['longitude']));
  String s = d.toStringAsFixed(2);
  if (d > 1) {
    s = s + " Km";
    return s;
  }
  if (d > 0.1) {
    s = (d * 1000).truncate().toString() + " m";
    return s;
  }
  return "próximo";
}

comparaD(DocumentSnapshot doc, double raio) {
  GeoPoint g = doc['localizacao']['geopoint'];
  double d = ponto.GeoFirePoint.distanceBetween(
      to: ponto.Coordinates(g.latitude, g.longitude),
      from: ponto.Coordinates(Authentication.minhaLocalizacao['latitude'],
          Authentication.minhaLocalizacao['longitude']));
  if (raio < 0) return true;
  if (d < raio) {
    print(doc['nome']);
    print(d.toStringAsPrecision(2));
    return true;
  }
  return false;
}

comparaI(DocumentSnapshot doc, int idade) {
  DateTime data = DateTime.parse(doc["data"]);
  int i = DateTime.now().year - data.year;
  if (idade < 0) return true;
  if (i <= idade) return true;
  return false;
}

getposition() async {
  Location loc = new Location();
  Map<String, double> mylocation =
      (await loc.getLocation()) as Map<String, double>;
  Authentication.minhaLocalizacao = mylocation;
  print("latitude");
  print(Authentication.minhaLocalizacao['latitude']);
}

assignLocal(DocumentSnapshot pet, BuildContext context) async {
  double lat = pet["localizacao"]["geopoint"].latitude;
  double long = pet["localizacao"]["geopoint"].longitude;

  final cod = new Coordinates(lat, long);
  List<Address> addresses =
      await Geocoder.local.findAddressesFromCoordinates(cod);
  Address first = addresses.first;
  PerfilPet.endereco = first.addressLine;
  Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) => PerfilPet(
            pet: pet,
          )));
  return;
}

class HomeWidget extends StatelessWidget {
  static String tag = "home-page";

  static double distancia = -1;
  static int sexo = -1;
  static int tipo = -1;
  static int idade = -1;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LayoutBloc>(
      bloc: LayoutBloc(context),
      child: _HomeWidgetState(),
    );
  }
}

class _HomeWidgetState extends StatelessWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _cnome = TextEditingController();
  TextEditingController _cdata = TextEditingController();
  TextEditingController _clocalizacao = TextEditingController();
  TextEditingController _craca = TextEditingController();
  TextEditingController _cvacinas = TextEditingController();
  TextEditingController _cdescricao = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final baseTextStyle = const TextStyle(fontFamily: 'Poppins');
    final regularTextStyle = baseTextStyle.copyWith(
        color: Layout.font(), fontSize: 14.0, fontWeight: FontWeight.w400);
    final subHeaderTextStyle = regularTextStyle.copyWith(fontSize: 17.0);
    final headerTextStyle = baseTextStyle.copyWith(
        color: Layout.font(), fontSize: 25.0, fontWeight: FontWeight.w600);

    _filtrosAtivos() {
      List<Widget> f = List();
      f.add(Container());
      if (FiltroD.selecionado != null) {
        f.add(Chip(label: Text(FiltroD.selecionado.nome)));
      }
      if (FiltroS.selecionado != null) {
        f.add(Chip(label: Text(FiltroS.selecionado.nome)));
      }
      if (FiltroT.selecionado != null) {
        f.add(Chip(label: Text(FiltroT.selecionado.nome)));
      }
      if (FiltroI.selecionado != null) {
        f.add(Chip(label: Text(FiltroI.selecionado.nome)));
      }
      return Wrap(
        spacing: 15,
        alignment: WrapAlignment.center,
        children: f,
      );
    }

    _petImage(DocumentSnapshot doc) {
      if (doc["foto"] == "")
        return Container(
          margin: EdgeInsets.symmetric(vertical: 0),
          alignment: FractionalOffset.centerLeft,
          height: 130.0,
          width: 80.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            image: DecorationImage(
              image: AssetImage("assets/img/sem_foto.png"),
              fit: BoxFit.fill,
            ),
          ),
        );
      return Container(
        margin: EdgeInsets.symmetric(vertical: 0),
        alignment: FractionalOffset.centerLeft,
        height: 130.0,
        width: 80.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          image: DecorationImage(
            image: NetworkImage(doc["foto"]),
            fit: BoxFit.fill,
          ),
        ),
      );
    }

    _petCard(DocumentSnapshot doc) => GestureDetector(
          child: Container(
            height: 130.0,
            child: Container(
              margin: EdgeInsets.fromLTRB(90.0, 10.0, 16.0, 16.0),
              constraints: BoxConstraints.expand(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(height: 3.0),
                  Row(
                    children: <Widget>[
                      Text(
                        title(doc['nome'].toString()),
                        style: headerTextStyle,
                      ),
                      Container(
                        width: 10,
                      ),
                      Text(
                        doc['sexo'] == 1 ? "Fêmea" : "Macho",
                        style: subHeaderTextStyle,
                      ),
                    ],
                  ),
                  Text(doc['raca'], style: subHeaderTextStyle),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 8.0),
                      height: 2.0,
                      width: 25.0,
                      color: Layout.primary()),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Row(children: <Widget>[
                          Image.asset(
                            'assets/img/ic_distance.png',
                            height: 15,
                          ),
                          Container(width: 8.0),
                          Text(distancia(doc), style: regularTextStyle),
                        ]),
                      )
                    ],
                  ),
                ],
              ),
            ),
            decoration: new BoxDecoration(
              color: Layout.card(),
              shape: BoxShape.rectangle,
              borderRadius: new BorderRadius.circular(8.0),
              boxShadow: <BoxShadow>[
                new BoxShadow(
                  color: Colors.black12,
                  blurRadius: 10.0,
                  offset: new Offset(0.0, 10.0),
                ),
              ],
            ),
          ),
          onTap: () {
            assignLocal(doc, context);
          },
        );

    _more(DocumentSnapshot doc) => Container(
          margin: EdgeInsets.fromLTRB(0, 95, 12, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  assignLocal(doc, context);
                },
                child: Icon(FontAwesomeIcons.ellipsisH),
              )
            ],
          ),
        );

    _petRow(DocumentSnapshot doc) => Container(
        height: 130.0,
        margin: const EdgeInsets.symmetric(
          vertical: 12.0,
          horizontal: 24.0,
        ),
        child: new Stack(
          children: <Widget>[
            _petCard(doc),
            _petImage(doc),
            //_more(doc),
          ],
        ));

    _streamQuery() {
      Query q = Firestore.instance
          .collection('pets')
          .orderBy('created', descending: true);

      if (!(HomeWidget.sexo < 0))
        q = q.where('sexo', isEqualTo: HomeWidget.sexo);
      if (HomeWidget.tipo > -1) q = q.where('tipo', isEqualTo: HomeWidget.tipo);
      if (q.snapshots() == null)
        print("!!!!!!!!!!!!!!!!!!!! nul !!!!!!!!!!!!!1");

      return Expanded(
        child: StreamBuilder(
          stream: q.snapshots(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (!snapshot.hasData)
              return Center(
                  child: Text(
                "Não existem pets com as características informadas",
                style: regularTextStyle,
              ));
            return ListView.builder(
              itemCount: snapshot.data.documents.length,
              itemBuilder: (BuildContext context, int index) {
                DocumentSnapshot doc = snapshot.data.documents[index];
                if (comparaD(doc, HomeWidget.distancia) &&
                    comparaI(doc, HomeWidget.idade)) {
                  return _petRow(doc);
                }
                return Container();
              },
            );
          },
        ),
      );
    }

    final content = Column(
      children: <Widget>[
        Container(
          height: 5,
        ),
        _filtrosAtivos(),
        _streamQuery(),
      ],
    );

    return Layout.getContent(context, content, HomeWidget.tag);
  }
}

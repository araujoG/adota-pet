import 'package:adota_pet/src/pages/chips-filtro.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:adota_pet/src/layout/layout-bloc.dart';
import 'package:adota_pet/src/layout/layout.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'home-widget.dart';

class SettingsPage extends StatelessWidget {
  static String tag = "settings-page";

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LayoutBloc>(
      bloc: LayoutBloc(context),
      child: _SettingsPageState(),
    );
  }
}

class _SettingsPageState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final baseTextStyle = const TextStyle(fontFamily: 'Poppins');
    final regularTextStyle = baseTextStyle.copyWith(
        color: Layout.font(), fontSize: 14.0, fontWeight: FontWeight.w400);
    final subHeaderTextStyle = regularTextStyle.copyWith(fontSize: 17.0);
    final headerTextStyle = baseTextStyle.copyWith(
        color: Layout.font(), fontSize: 25.0, fontWeight: FontWeight.w600);

    _filtrosLocal() {
      List<FiltroD> distancias = [
        FiltroD(5),
        FiltroD(10),
        FiltroD(20),
        FiltroD(30),
        FiltroD(40),
        FiltroD(50),
        FiltroD(100),
        FiltroD(200),
        FiltroD(300),
      ];
      return FiltroDistancia(distancias);
    }

    _aplicarFiltros() => RaisedButton(
          child: Text(
            "Aplicar Filtros",
            style: TextStyle(color: Layout.white()),
          ),
          onPressed: () {
            HomeWidget.distancia = FiltroD.selecionado.valor;
            Navigator.of(context).pop();
            Navigator.of(context).popAndPushNamed(HomeWidget.tag);
          },
          color: Layout.secundary(),
        );

    showAlertDialog1(BuildContext context) {
      // configura o button
      Widget okButton = FlatButton(
        child: Text("OK"),
        onPressed: () {},
      );

      // configura o  AlertDialog
      AlertDialog alerta = AlertDialog(
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(
                    "Localização: ",
                    style: headerTextStyle,
                  )
                ],
              ),
              _filtrosLocal(),
            ],
          ),
        ),
        actions: [_aplicarFiltros()],
      );

      // exibe o dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alerta;
        },
      );
    }

    _limparFiltros() => RaisedButton(
          child: Text(
            "Limpar Filtros",
            style: TextStyle(color: Layout.white()),
          ),
          onPressed: () {
            HomeWidget.distancia = -1;
            FiltroD.selecionado = null;
            Navigator.of(context).pop();
            Navigator.of(context).popAndPushNamed(HomeWidget.tag);
          },
          color: Layout.accent(),
        );

    final content = Container(
      padding: EdgeInsets.all(15),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Localização: ",
                style: headerTextStyle,
              )
            ],
          ),
          _filtrosLocal(),
          Divider(),
          Row(
            children: <Widget>[
              _limparFiltros(),
              Container(
                width: 10,
              ),
              _aplicarFiltros()
            ],
          )
        ],
      ),
    );

    return Layout.getContent(context, content, "");
  }
}

import 'package:adota_pet/src/login/login-widget.dart';
import 'package:adota_pet/src/services/authentication/Authentication.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';

class HomeBloc extends BlocBase {

  final _authentication = new Authentication();
  
  final BuildContext context;
  HomeBloc(this.context);


  onClickLogoutGoogle() async{
    await _authentication.signOutWithGoogle();
    Navigator.pushReplacement(context, 
      MaterialPageRoute(
        builder: (BuildContext context) => LoginWidget()
      )
    );
  }

  @override
  void dispose() {
  }
  
}
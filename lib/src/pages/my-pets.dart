import 'dart:io';

import 'package:adota_pet/src/layout/layout-bloc.dart';
import 'package:adota_pet/src/layout/layout.dart';
import 'package:adota_pet/src/pages/editar-pet.dart';
import 'package:adota_pet/src/pages/home-widget.dart';
import 'package:adota_pet/src/pages/pet-profile.dart';
import 'package:adota_pet/src/services/authentication/Authentication.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum ListAction { editar, deletar }


class MyPetsWidget extends StatelessWidget {
  static String tag = "mypets-page";

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LayoutBloc>(
      bloc: LayoutBloc(context),
      child: _MyPetsWidgetState(),
    );
  }
}

class _MyPetsWidgetState extends StatelessWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _cnome = TextEditingController();
  TextEditingController _cdata = TextEditingController();
  TextEditingController _clocalizacao = TextEditingController();
  TextEditingController _craca = TextEditingController();
  TextEditingController _cvacinas = TextEditingController();
  TextEditingController _cdescricao = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final baseTextStyle = const TextStyle(fontFamily: 'Poppins');
    final regularTextStyle = baseTextStyle.copyWith(
        color:  Layout.font(),
        fontSize: 14.0,
        fontWeight: FontWeight.w400);
    final subHeaderTextStyle = regularTextStyle.copyWith(fontSize: 17.0);
    final headerTextStyle = baseTextStyle.copyWith(
        color:  Layout.font(), fontSize: 25.0, fontWeight: FontWeight.w600);

    _petImage(DocumentSnapshot doc) => Container(
      margin: EdgeInsets.symmetric(vertical: 0),
      alignment: FractionalOffset.centerLeft,
      height: 130.0,
      width: 80.0,
      
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        image: DecorationImage(
          image: NetworkImage(doc["foto"]),
          fit: BoxFit.fill,
        ),
      ),
    ); 
    

    _petCard(DocumentSnapshot doc) => GestureDetector(
      child: Container(
          height: 130.0,
          child: Container(
            margin: EdgeInsets.fromLTRB(90.0, 10.0, 16.0, 16.0),
            constraints: BoxConstraints.expand(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(height: 3.0),
                Row(
                  children: <Widget>[
                    Text(
                      title(doc['nome'].toString()),
                      style: headerTextStyle,
                    ),
                    Container(
                      width: 10,
                    ),
                    Text(
                      doc['sexo'] == 1 ? "Fêmea":"Macho",
                      style: subHeaderTextStyle,
                    ),
                  ],
                ),
                Text(doc['raca'], style: subHeaderTextStyle),
                Container(
                    margin: EdgeInsets.symmetric(vertical: 8.0),
                    height: 2.0,
                    width: 25.0,
                    color: Layout.primary()),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Row(children: <Widget>[
                        Image.asset(
                          'assets/img/ic_distance.png',
                          height: 15,
                        ),
                        Container(width: 8.0),
                        Text(distancia(doc), style: regularTextStyle),
                      ]),
                    )
                  ],
                ),
              ],
            ),
          ),
          decoration: new BoxDecoration(
            color: Layout.card(),
            shape: BoxShape.rectangle,
            borderRadius: new BorderRadius.circular(8.0),
            boxShadow: <BoxShadow>[
              new BoxShadow(
                color: Colors.black12,
                blurRadius: 10.0,
                offset: new Offset(0.0, 10.0),
              ),
            ],
          ),
        ),
        onTap: (){assignLocal(doc, context);},
    );
    
    _more(DocumentSnapshot doc) => Container(
          margin: EdgeInsets.fromLTRB(0, 95, 12, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  assignLocal(doc,context);
                },
                child: Icon(FontAwesomeIcons.ellipsisH),
              )
            ],
          ),
        );

    _options(DocumentSnapshot doc) => Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            PopupMenuButton<ListAction>(
              itemBuilder: (BuildContext context) {
                return <PopupMenuEntry<ListAction>>[
                  PopupMenuItem<ListAction>(
                    value: ListAction.deletar,
                    child: Row(
                      children: <Widget>[
                        Icon(FontAwesomeIcons.trashAlt),
                        Text("  Excluir")
                      ],
                    ),
                  ),
                  PopupMenuItem<ListAction>(
                    value: ListAction.editar,
                    child: Row(
                      children: <Widget>[
                        Icon(FontAwesomeIcons.pencilAlt),
                        Text("  Editar")
                      ],
                    ),
                  )
                ];
              },
              onSelected: (ListAction result) {
                switch (result) {
                  case ListAction.deletar:
                    doc.reference.delete();
                    break;
                  case ListAction.editar:
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => EditarPet(
                              doc: doc,
                            )));
                    break;
                }
              },
            ),
          ],
        ));
        

    _petRow(DocumentSnapshot doc) => Container(
        height: 130.0,
        margin: const EdgeInsets.symmetric(
          vertical: 12.0,
          horizontal: 24.0,
        ),
        child: new Stack(
          children: <Widget>[
            _petCard(doc),
            _petImage(doc),
            _options(doc),
            //_more(doc)
          ],
        ));

    final content = StreamBuilder(
      stream: Firestore.instance
          .collection('pets')
          .where('uid', isEqualTo: Authentication.usuarioLogado.uid)
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData)
          return const Center(child: CircularProgressIndicator());
        if (snapshot.data.documents.length == 0) {
          return const Center(
            child: Text(
              "Você não tem pets cadastrados",
              style: TextStyle(fontFamily: 'Poppins', fontSize: 15),
            ),
          );
        }
        return ListView.builder(
          itemCount: snapshot.data.documents.length,
          itemBuilder: (BuildContext context, int index) {
            DocumentSnapshot doc = snapshot.data.documents[index];
            return _petRow(doc);
          },
        );
      },
    );

    return Layout.getContent(context, content, MyPetsWidget.tag);
  }
}

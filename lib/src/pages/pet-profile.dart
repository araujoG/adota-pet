import 'dart:io';
import 'package:adota_pet/src/layout/layout-bloc.dart';
import 'package:adota_pet/src/layout/layout.dart';
import 'package:adota_pet/src/pages/home-widget.dart';
import 'package:adota_pet/src/services/authentication/Authentication.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geocoder/geocoder.dart';

_infoContato(BuildContext context, String uid) async {
  final baseTextStyle = const TextStyle(fontFamily: 'Poppins');
  final regularTextStyle = baseTextStyle.copyWith(
      color: Layout.font(), fontSize: 16.0, fontWeight: FontWeight.w400);
  final headerTextStyle = baseTextStyle.copyWith(
      color: Layout.font(), fontSize: 25.0, fontWeight: FontWeight.w600);

  DocumentSnapshot doc =
      await Firestore.instance.collection('usuarios').document(uid).get();
  AlertDialog displayContato = AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                image: DecorationImage(
                  image: NetworkImage(doc["foto"]),
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            Center(
              child: Text(title(doc['nome']), style: headerTextStyle),
            ),
            Row(children: <Widget>[
              Icon(FontAwesomeIcons.at),
              Text(":  ", style: headerTextStyle),
              Expanded(
                child: Text(
                  doc['email'],
                  maxLines: 2,
                  style: regularTextStyle,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ]),
            Row(children: <Widget>[
              Icon(FontAwesomeIcons.phoneSquareAlt),
              Text(":  ", style: headerTextStyle),
              Expanded(
                child: Text(
                  doc['telefone'],
                  maxLines: 2,
                  style: regularTextStyle,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ]),
          ],
        ),
      ));
  showDialog(
    context: context,
    builder: (BuildContext ctx) {
      return displayContato;
    },
  );
}

class PerfilPet extends StatelessWidget {
  DocumentSnapshot pet;
  static String endereco;
  PerfilPet({Key key, this.pet}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider<LayoutBloc>(
      bloc: LayoutBloc(context),
      child: _PerfilPetState(
        pet: pet,
      ),
    );
  }
}

class _PerfilPetState extends StatelessWidget {
  DocumentSnapshot pet;
  _PerfilPetState({Key key, this.pet}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final baseTextStyle = const TextStyle(fontFamily: 'Poppins');
    final regularTextStyle = baseTextStyle.copyWith(
        color: Layout.font(), fontSize: 20.0, fontWeight: FontWeight.w400);
    final headerTextStyle = baseTextStyle.copyWith(
        color: Layout.font(), fontSize: 25.0, fontWeight: FontWeight.w600);

    String id = pet["uid"];
    String urlFoto = pet['foto'];

    displayFoto() {
      if (pet['foto'] != "") {
        return Container(
          height: 240,
          width: 120,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              image: DecorationImage(
                  image: NetworkImage(urlFoto), fit: BoxFit.contain)),
        );
      }
      return Container(
        height: 240,
        width: 120,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            image: DecorationImage(
                image: AssetImage("assets/img/sem_foto.png"),
                fit: BoxFit.contain)),
      );
    }

    displayString(String label) => Row(
          children: <Widget>[
            Text(
              title(label),
              style: headerTextStyle,
            ),
            Text(
              ": ",
              style: headerTextStyle,
            ),
            Expanded(
              child: Text(
                pet[label],
                style: regularTextStyle,
              ),
            ),
          ],
        );

    displayLocal() {
      return Row(
        children: <Widget>[
          Expanded(
            child: Text(
              PerfilPet.endereco,
              style: regularTextStyle,
            ),
          ),
        ],
      );
    }

    displayTitle(String label) => Row(
          children: <Widget>[
            Text(
              title(label),
              style: headerTextStyle,
            ),
            Text(
              ": ",
              style: headerTextStyle,
            ),
          ],
        );

    displayMultiLine(String label) => Flexible(
          fit: FlexFit.loose,
          child: Text(
            pet[label],
            style: regularTextStyle,
            overflow: TextOverflow.clip,
            textDirection: TextDirection.ltr,
          ),
        );

    displaySexo() {
      if (pet["sexo"] == 0) {
        return Row(
          children: <Widget>[
            Text(
              title("Sexo"),
              style: headerTextStyle,
            ),
            Text(": "),
            Text(
              "macho",
              style: regularTextStyle,
            )
          ],
        );
      }
      return Row(
        children: <Widget>[
          Text(
            title("Sexo"),
            style: headerTextStyle,
          ),
          Text(": "),
          Text(
            "fêmea",
            style: regularTextStyle,
          )
        ],
      );
    }

    displayTipo() => Row(
          children: <Widget>[
            Text(
              title("Tipo: "),
              style: headerTextStyle,
            ),
            Expanded(
              child: Text(
                Tipos[pet["tipo"]],
                style: regularTextStyle,
              ),
            ),
          ],
        );

    var interesses = Firestore.instance.collection("interests");

    final interestButton = Center(
        child: IconButton(
      iconSize: 50,
      icon: Icon(FontAwesomeIcons.heart),
      onPressed: () {
        interesses.add({
          "uid": Authentication.usuarioLogado.uid,
          "uemail": Authentication.usuarioLogado.email,
          "uname": Authentication.usuarioLogado.displayName,
          "pet": pet.documentID
        });
      },
    ));

    showAlertDialog(context, String name) {
      // configura o  AlertDialog
      AlertDialog alerta = AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(
                    "Confirmação",
                    style: Layout.headerTextStyle,
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                    "Você tem certeza que deseja doar o seu pet para " +
                        name +
                        "?",
                  ))
                ],
              ),
            ],
          ),
        ),
        actions: [
          RaisedButton(
              color: Layout.secundary(),
              child: Text("Sim, Eu tenho certeza",
                  style: TextStyle(color: Layout.white())),
              onPressed: () {
                pet.reference.delete();
                Navigator.pop(context);
                Navigator.pushNamed(context, "home-page");
              }),
          RaisedButton(
              color: Layout.accent(),
              child: Text("Não", style: TextStyle(color: Layout.white())),
              onPressed: () {
                Navigator.pop(context);
              })
        ],
      );

      // exibe o dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alerta;
        },
      );
    }

    final _showInterest = StreamBuilder(
      stream: interesses.where("pet", isEqualTo: pet.documentID).snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData)
          return const Center(child: CircularProgressIndicator());
        if (id == Authentication.usuarioLogado.uid) {
          if (snapshot.data.documents.length == 0) {
            return const Center(
              child: Text(
                "Nenhuma pessoa se interessou ainda",
                style: TextStyle(fontFamily: 'Poppins', fontSize: 15),
              ),
            );
          }
          return Column(
            children: <Widget>[
              Text(
                "Interessados",
                style: headerTextStyle,
              ),
              ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: snapshot.data.documents.length,
                itemBuilder: (BuildContext context, int index) {
                  DocumentSnapshot doc = snapshot.data.documents[index];
                  return Center(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            GestureDetector(
                              child: Text(
                                doc["uname"],
                                style: regularTextStyle,
                                textAlign: TextAlign.justify,
                              ),
                              onTap: () {
                                _infoContato(context, doc['uid']);
                              },
                            ),
                            Container(width: 10),
                            RaisedButton(
                              padding: EdgeInsets.all(0),
                              color: Color.fromRGBO(255, 119, 124, 1),
                              child: Text(
                                "Doar",
                                style: regularTextStyle,
                              ),
                              onPressed: () {
                                showAlertDialog(context, doc["uname"]);
                                //pet.reference.delete();
                              },
                            )
                          ],
                        ),
                      ],
                    ),
                  );
                },
              )
            ],
          );
          ;
        }
        return StreamBuilder(
            stream: interesses
                .where("pet", isEqualTo: pet.documentID)
                .where("uid", isEqualTo: Authentication.usuarioLogado.uid)
                .snapshots(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (!snapshot.hasData)
                return const Center(child: CircularProgressIndicator());
              if (snapshot.data.documents.length == 0)
                return interestButton;
              else
                return Center(
                    child: IconButton(
                  iconSize: 50,
                  icon: Icon(FontAwesomeIcons.solidHeart),
                  onPressed: () {
                    snapshot.data.documents[0].reference.delete();
                  },
                ));
            });
      },
    );
  displayDono() => Row(
    children: <Widget>[
      FlatButton(
        padding: EdgeInsets.all(0),
        child: Text(
          title("Perfil Dono"),
          style: headerTextStyle,
        ),
        onPressed: (){_infoContato(context, pet["uid"]);},
      )
    ],
  );

    coluna() {
      return Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              height: 15,
            ),
            displayDono(),
            displayString("nome"),
            displaySexo(),
            displayTipo(),
            displayString("raca"),
            displayString("data"),
            displayTitle("Endereço"),
            displayLocal(),
            displayTitle("vacinas"),
            displayMultiLine("vacinas"),
            displayTitle("descricao"),
            displayMultiLine("descricao"),
          ],
        ),
        width: MediaQuery.of(context).size.width - 150,
        padding: EdgeInsets.only(left: 15),
      );
    }

    final content = Container(
      child: SingleChildScrollView(
        padding: EdgeInsets.all(15),
        child: ListBody(
          children: <Widget>[displayFoto(), coluna(), _showInterest],
        ),
      ),
    );
    return Layout.getContent(context, content, "");
  }
}

import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:path/path.dart';

import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class UploadPage extends StatefulWidget {
  static String tag = "upload-page";

  @override
  _UploadPageState createState() => _UploadPageState();
}

class _UploadPageState extends State<UploadPage> {
  File sampleImage;

  Future getImage() async {
    this.sampleImage = await ImagePicker.pickImage(source: ImageSource.gallery);
  }

  Future uploadPic(BuildContext context) async {
    String fileName = basename(sampleImage.path);
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(sampleImage);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    String t = await taskSnapshot.ref.getDownloadURL();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: sampleImage == null
            ? Text("Selecione uma imagem")
            : enableUpload(context),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: "Adiocionar imagem",
        child: Icon(Icons.add),
      ),
    );
  }

  Widget enableUpload(context) {
    return Container(
      child: Column(
        children: <Widget>[
          Image.file(sampleImage, height: 300.0, width: 300.0),
          RaisedButton(
            elevation: 7.0,
            child: Text("Upload"),
            textColor: Colors.white,
            color: Colors.blue,
            onPressed: () async {
              uploadPic(context);
            },
          )
        ],
      ),
    );
  }
}

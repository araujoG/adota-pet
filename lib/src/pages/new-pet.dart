import 'dart:io';
import 'dart:ui';
import 'package:adota_pet/src/pages/map.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:intl/intl.dart';
import 'package:adota_pet/src/layout/layout.dart';
import 'package:adota_pet/src/pages/home-widget.dart';
import 'package:adota_pet/src/services/authentication/Authentication.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location/location.dart';
import 'package:path/path.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

_validar(int tipo, int sexo) {
  if (tipo == null)
    FormularioPetWidget.tipoValido = false;
  else
    FormularioPetWidget.tipoValido = true;
  if (sexo == null)
    FormularioPetWidget.sexoValido = false;
  else
    FormularioPetWidget.sexoValido = true;
  if (FireMapState.point == null)
    FormularioPetWidget.localValido = false;
  else
    FormularioPetWidget.localValido = true;
}

_resetValida() {
  FormularioPetWidget.localValido = true;
  FormularioPetWidget.sexoValido = true;
  FormularioPetWidget.tipoValido = true;
}

class FormularioPetWidget extends StatefulWidget {
  static String tag = "newPet-page";

  static bool localValido = true;
  static bool sexoValido = true;
  static bool tipoValido = true;
  FormularioPetWidget({Key key}) : super(key: key);

  @override
  _FormularioPetWidgetState createState() => _FormularioPetWidgetState();
}

class _FormularioPetWidgetState extends State<FormularioPetWidget> {
  final _formKey = GlobalKey<FormState>();

  File sampleImage;
  String nomeImagem;
  bool uploading = false;
  bool inicio = true;


  Future getImage() async {
    this.sampleImage = await ImagePicker.pickImage(source: ImageSource.gallery);
    this.nomeImagem = basename(sampleImage.path);
    setState(() {
      this.sampleImage = this.sampleImage;
      this.nomeImagem = this.nomeImagem;
    });
  }

  Future<dynamic> uploadPic(BuildContext context) async {
    this.uploading = true;
    String fileName = basename(this.sampleImage.path);
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(sampleImage);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    return taskSnapshot.ref.getDownloadURL();
  }

  TextEditingController _cnome = TextEditingController();
  TextEditingController _cdata = TextEditingController();
  TextEditingController _clocalizacao = TextEditingController();
  TextEditingController _craca = TextEditingController();
  TextEditingController _cvacinas = TextEditingController();
  TextEditingController _cdescricao = TextEditingController();
  int _sexo;
  int _tipo;
  String urlFoto;

  @override
  Widget build(BuildContext context) {
    if(this.inicio){
      _resetValida();
      FireMapState.point = null;
      this.inicio = false;
    }
    final baseTextStyle = const TextStyle(fontFamily: 'Poppins');
    final regularTextStyle =
        baseTextStyle.copyWith(fontSize: 14.0, fontWeight: FontWeight.w400);
    final warningTextStyle = baseTextStyle.copyWith(
        color: Colors.red, fontSize: 14.0, fontWeight: FontWeight.w400);

    final inputNome = TextFormField(
      controller: _cnome,
      autofocus: true,
      decoration: InputDecoration(
        hintStyle: regularTextStyle,
        labelStyle: regularTextStyle,
        hintText: 'Nome',
        labelText: 'Nome',
        contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
        ),
      ),
      validator: (value) {
        if (value.isEmpty) {
          return "Obrigatório";
        }
        return null;
      },
    );

    final inputLocalizacao = RaisedButton(
        child: Text(
          "Localização",
          style: regularTextStyle,
        ),
        onPressed: () {
          getposition();
          Navigator.of(context).pushNamed("map");
        });

    _inputData() {
      final format = DateFormat("yyyy-MM-dd");
      return DateTimeField(
        controller: _cdata,
        enabled: true,
        decoration: InputDecoration(
          labelText: 'Data de Nascimento',
          hintText: 'Data de Nascimento',
          hintStyle: regularTextStyle,
          labelStyle: regularTextStyle,
          contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
          ),
        ),
        format: format,
        onShowPicker: (context, currentValue) {
          return showDatePicker(
            context: context,
            firstDate: DateTime(2000),
            initialDate: currentValue ?? DateTime.now(),
            lastDate: DateTime.now(),
            builder: (BuildContext context, Widget child) {
              return Theme(
                data: ThemeData(
                    accentColor: Layout.accent(),
                    primaryColor: Layout.primary()),
                child: child,
              );
            },
          );
        },
        validator: (DateTime value) {},
      );
    }

    final inputRaca = TextFormField(
      controller: _craca,
      keyboardType: TextInputType.text,
      decoration: new InputDecoration(
        hintStyle: regularTextStyle,
        labelStyle: regularTextStyle,
        labelText: 'Raça',
        hintText: 'Raça',
        contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
        ),
      ),
      validator: (value) {
        if (value.isEmpty) {
          return "Obrigatório";
        }
      },
    );

    final inputVacinas = TextFormField(
      controller: _cvacinas,
      keyboardType: TextInputType.text,
      decoration: new InputDecoration(
        labelText: 'Vacinas',
        hintText: 'Vacinas',
        hintStyle: regularTextStyle,
        labelStyle: regularTextStyle,
        contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
        ),
      ),
    );

    final inputDescricao = TextFormField(
      controller: _cdescricao,
      maxLines: 5,
      keyboardType: TextInputType.multiline,
      decoration: InputDecoration(
        hintText: 'Descrição',
        labelText: 'Descrição',
        hintStyle: regularTextStyle,
        labelStyle: regularTextStyle,
        alignLabelWithHint: true,
        contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
        ),
      ),
    );

    _inputSexo() {
      List<DropdownMenuItem<int>> genderList = [];
      genderList.add(DropdownMenuItem(
        child: new Text(
          'Macho',
          style: regularTextStyle,
        ),
        value: 0,
      ));
      genderList.add(DropdownMenuItem(
        child: new Text(
          'Fêmea',
          style: regularTextStyle,
        ),
        value: 1,
      ));
      return DropdownButton(
        hint: new Text(
          'Sexo do Pet',
          style: regularTextStyle,
        ),
        items: genderList,
        value: _sexo,
        onChanged: (value) {
          setState(() {
            _sexo = value;
          });
        },
      );
    }

    _warning() {
      String s = "";
      if (!FormularioPetWidget.sexoValido) s = "O campo sexo é obrigatório\n";
      if (!FormularioPetWidget.tipoValido) s += "O campo tipo é obrigatório\n";
      if (!FormularioPetWidget.localValido)
        s += "Por favor, selecione uma localização\n";
      if (s != "")
        return Text(
          s,
          style: warningTextStyle,
        );
      return Container();
    }

    _item(String s, int i) {
      return DropdownMenuItem(
          child: Text(
            s,
            style: regularTextStyle,
          ),
          value: i);
    }

    _inputTipo() {
      List<DropdownMenuItem<int>> typeList = [];
      int i = 0;
      for (var item in Tipos) {
        typeList.add(_item(item, i++));
      }

      return DropdownButton(
        hint: Text(
          'Tipo do Pet',
          style: regularTextStyle,
        ),
        items: typeList,
        value: _tipo,
        onChanged: (value) {
          setState(() {
            _tipo = value;
          });
        },
      );
    }

    final inputFoto = Container(
        child: this.sampleImage == null
            ? Row(
                children: <Widget>[
                  FlatButton.icon(
                    icon: Icon(FontAwesomeIcons.upload),
                    label: Text(
                      "Selecione uma imagem",
                      overflow: TextOverflow.clip,
                      maxLines: 2,
                      style: regularTextStyle,
                    ),
                    onPressed: () async {
                      getImage();
                    },
                  ),
                  
                ],
              )
            : Row(
                children: <Widget>[
                  IconButton(
                    onPressed: () async {
                      getImage();
                    },
                    icon: Icon(FontAwesomeIcons.upload),
                  ),
                  Text(
                    basename(this.sampleImage.path),
                    style: regularTextStyle,
                  ),
                ],
              ));

    final espaco = Container(
      height: 15,
    );

    _loadingCircle() {
      if (uploading) return CircularProgressIndicator();
      return Container();
    }

    coluna() {
      return Container(
        child: Column(
          children: <Widget>[
            inputNome,
            espaco,
            _inputData(),
            espaco,
            inputRaca,
            espaco,
            inputLocalizacao,
          ],
        ),
        width: MediaQuery.of(context).size.width - 150,
        padding: EdgeInsets.only(left: 15),
      );
    }

    displayFoto() {
      if (sampleImage == null) {
        if (urlFoto != null) {
          return Container(
            height: 240,
            width: 120,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                image: DecorationImage(
                    image: NetworkImage(urlFoto), fit: BoxFit.contain)),
          );
        }
        return Container(
          height: 240,
          width: 120,
        );
      }

      return Container(
        child: Column(
          children: <Widget>[
            Image.file(
              sampleImage,
              height: 240,
              width: 120,
              fit: BoxFit.contain,
            )
          ],
        ),
      );
    }

    final botaoCancelar = RaisedButton(
      color: Layout.accent(),
      child: Text(
        "Cancelar",
        style: TextStyle(color: Layout.white()),
      ),
      onPressed: () {
        _resetValida() {}
        Navigator.of(context).pop();
      },
    );

    final botaoSalvar = RaisedButton(
      child: Text(
        'Salvar',
        style: TextStyle(color: Colors.white),
      ),
      onPressed: () async {
        if (!this.uploading) {
          _validar(_tipo, _sexo);
          setState(() {});
          if (_formKey.currentState.validate() &&
              FormularioPetWidget.localValido &&
              FormularioPetWidget.sexoValido &&
              FormularioPetWidget.tipoValido) {
            String foto = "";
            uploading = true;
            setState(() {});
            if (sampleImage != null) foto = await uploadPic(context);
            Firestore.instance.collection('pets').add({
              'created': DateTime.now().toString(),
              'uid': Authentication.usuarioLogado.uid,
              'uemail': Authentication.usuarioLogado.email,
              'nome': _cnome.text,
              'data': _cdata.text,
              'raca': _craca.text,
              'vacinas': _cvacinas.text,
              'descricao': _cdescricao.text,
              'sexo': _sexo,
              'tipo': _tipo,
              'foto': foto,
              'localizacao': FireMapState.point.data
            });
            this.sampleImage = null;
            this.nomeImagem = null;
            
            while (Navigator.canPop(context)) {
              Navigator.of(context).pop();
            }
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => HomeWidget()));
          }
        }
      },
      color: Layout.secundary(),
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Layout.primary(),
        title: Text(
          "Adota Pet",
          style: TextStyle(
            fontSize: 25,
            color: Layout.white(),
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          padding: EdgeInsets.all(15),
          child: ListBody(
            children: <Widget>[
              Row(
                children: <Widget>[displayFoto(), coluna()],
              ),
              Wrap(
                alignment: WrapAlignment.spaceEvenly,
                children: <Widget>[_inputSexo(), _inputTipo()],
              ),
              espaco,
              inputVacinas,
              espaco,
              inputDescricao,
              espaco,
              inputFoto,
              espaco,
              _warning(),
              Row(
                children: <Widget>[
                  botaoCancelar,
                  Container(width: 15),
                  botaoSalvar
                ],
                mainAxisAlignment: MainAxisAlignment.end,
              ),
              Container(
                height: 30,
                margin: EdgeInsets.only(top: 50),
                child: Center(child: _loadingCircle()),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class BasicDateTimeField extends StatelessWidget {
  final format = DateFormat("yyyy-MM-dd HH:mm");
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Text('Time picker (${format.pattern})'),
      DateTimeField(
        format: format,
        onShowPicker: (context, currentValue) async {
          final date = await showDatePicker(
              context: context,
              firstDate: DateTime(1900),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2100));
          if (date != null) {
            final time = await showTimePicker(
              context: context,
              initialTime:
                  TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
            );
            return DateTimeField.combine(date, time);
          } else {
            return currentValue;
          }
        },
      ),
    ]);
  }
}

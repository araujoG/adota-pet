import 'package:adota_pet/src/layout/layout.dart';
import 'package:adota_pet/src/pages/home-widget.dart';
import 'package:flutter/material.dart';

abstract class Filtro {
  String nome;
  var valor;
  String categoria;
  bool selected;
}

class FiltroD extends Filtro {
  static FiltroD selecionado;
  FiltroD(double v) {
    this.valor = v;
    this.nome = v.truncate().toString() + " Km";
    this.categoria = 'localizacao';
    this.selected = false;
  }
}

class FiltroI extends Filtro {
  static FiltroI selecionado;
  FiltroI(int v) {
    this.valor = v;
    if (v == 1)
      this.nome = v.toString()+ " ano";
    else if(v>1)
      this.nome = v.toString()+ " anos";
    this.categoria = 'Idade';
    this.selected = false;
  }
}

class FiltroS extends Filtro {
  static FiltroS selecionado;
  FiltroS(int v) {
    this.categoria = 'sexo';
    this.valor = v;
    if (v == 1)
      this.nome = "Fêmea";
    else
      this.nome = "Macho";
    this.selected = false;
  }
}

class FiltroT extends Filtro {
  static FiltroT selecionado;
  FiltroT(String s, int v) {
    this.categoria = 'tipo';
    this.valor = v;
    this.nome = s;
  }
}

class FiltroDistancia extends StatefulWidget {
  final List<Filtro> reportList;

  FiltroDistancia(this.reportList);

  @override
  _FiltroDistanciaState createState() => new _FiltroDistanciaState();
}

class _FiltroDistanciaState extends State<FiltroDistancia> {
  _buildChoiceList() {
    List<Widget> choices = List();
    widget.reportList.forEach((item) {
      choices.add(Container(
        child: ChoiceChip(
          label: Text(item.nome),
          labelStyle: TextStyle(
              color: Colors.black, fontSize: 14.0, fontWeight: FontWeight.bold),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          backgroundColor: Color(0xffededed),
          selectedColor: Layout.primary(),
          selected: FiltroD.selecionado != null
              ? item.valor == FiltroD.selecionado.valor
              : false,
          onSelected: (selected) {
            setState(() {
              FiltroD.selecionado = item;
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      alignment: WrapAlignment.spaceEvenly,
      spacing: 8.0,
      runSpacing: 3.0,
      children: _buildChoiceList(),
    );
  }
}

class FiltroSexo extends StatefulWidget {
  final List<Filtro> reportList;

  FiltroSexo(this.reportList);

  @override
  _FiltroSexoState createState() => new _FiltroSexoState();
}

class _FiltroSexoState extends State<FiltroSexo> {
  _buildChoiceList() {
    List<Widget> choices = List();
    widget.reportList.forEach((item) {
      choices.add(Container(
        child: ChoiceChip(
          label: Text(item.nome),
          labelStyle: TextStyle(
            color: Colors.black,
            fontSize: 14.0,
            fontWeight: FontWeight.bold,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          backgroundColor: Color(0xffededed),
          selectedColor: Layout.primary(),
          selected: FiltroS.selecionado != null
              ? item.valor == FiltroS.selecionado.valor
              : false,
          onSelected: (selected) {
            setState(() {
              FiltroS.selecionado = item;
              
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      alignment: WrapAlignment.spaceEvenly,
      spacing: 8.0,
      runSpacing: 3.0,
      children: _buildChoiceList(),
    );
  }
}

class FiltroTipo extends StatefulWidget {
  final List<Filtro> reportList;

  FiltroTipo(this.reportList);

  @override
  _FiltroTipoState createState() => new _FiltroTipoState();
}

class _FiltroTipoState extends State<FiltroTipo> {
  _buildChoiceList() {
    List<Widget> choices = List();
    widget.reportList.forEach((item) {
      choices.add(Container(
        child: ChoiceChip(
          label: Text(item.nome),
          labelStyle: TextStyle(
            color: Colors.black,
            fontSize: 14.0,
            fontWeight: FontWeight.bold,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          backgroundColor: Color(0xffededed),
          selectedColor: Layout.primary(),
          selected: FiltroT.selecionado != null
              ? item.valor == FiltroT.selecionado.valor
              : false,
          onSelected: (selected) {
            setState(() {
              FiltroT.selecionado = item;
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      alignment: WrapAlignment.spaceEvenly,
      spacing: 8.0,
      runSpacing: 3.0,
      children: _buildChoiceList(),
    );
  }
}

class FiltroIdade extends StatefulWidget {
  final List<Filtro> reportList;

  FiltroIdade(this.reportList);

  @override
  _FiltroIdadeState createState() => new _FiltroIdadeState();
}

class _FiltroIdadeState extends State<FiltroIdade> {
  _buildChoiceList() {
    List<Widget> choices = List();
    widget.reportList.forEach((item) {
      choices.add(Container(
        child: ChoiceChip(
          label: Text(item.nome),
          labelStyle: TextStyle(
            color: Colors.black,
            fontSize: 14.0,
            fontWeight: FontWeight.bold,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          backgroundColor: Color(0xffededed),
          selectedColor: Layout.primary(),
          selected: FiltroI.selecionado != null
              ? item.valor == FiltroI.selecionado.valor
              : false,
          onSelected: (selected) {
            setState(() {
              FiltroI.selecionado = item;
              
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      alignment: WrapAlignment.spaceEvenly,
      spacing: 8.0,
      runSpacing: 3.0,
      children: _buildChoiceList(),
    );
  }
}